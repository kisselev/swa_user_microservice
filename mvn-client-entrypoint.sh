#!/bin/bash

wait_for_discovery_service ()
{
    while true; do 
    echo "Sending request to: ${EUREKA_DISCOVERY_SERVICE}"
    curl -I ${EUREKA_DISCOVERY_SERVICE}

    if [ $? -eq 0 ]; then
        echo "discovery service is available ( ͡° ͜ʖ ͡°)"
        break
    fi
    echo "sleep 2 sec ... "
    sleep 2
    done
}

cd /projects/user_service

wait_for_discovery_service

mvn spring-boot:run