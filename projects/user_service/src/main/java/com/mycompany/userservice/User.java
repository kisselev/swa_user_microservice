package com.mycompany.userservice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

/**
 * Simple JavaBean domain object representing a veterinarian.
 *
 * @author Ken Krebs
 * @author Juergen Hoeller
 * @author Sam Brannen
 * @author Arjen Poutsma
 * @author Maciej Szarlinski
 */
@Entity
@Getter
@Setter
@Table(name = "user_table")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column(name = "firstname")
    @NotEmpty
    private String firstname;

    @Column(name = "lastname")
    @NotEmpty
    public String lastname;

    @Column(name = "address")
    @NotEmpty
    public String address;

    @Column(name = "phonenumber")
    @NotEmpty
    public String phonenumber;

    @Column(name = "registrationdate")
    @NotEmpty
    public String registrationdate;

    @Column(name = "paymentmethod")
    @NotEmpty
    public String paymentmethod;

}