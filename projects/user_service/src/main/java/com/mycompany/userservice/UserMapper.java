package com.mycompany.userservice;


import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.mycompany.userservice.api.model.UserDto;

@Mapper
public interface UserMapper {
    public static UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserDto mapTo(User wp);
    User mapTo(UserDto wpDto);
}