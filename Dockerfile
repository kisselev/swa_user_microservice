FROM maven:latest as base
WORKDIR /code
RUN apt update && apt install -y git iputils-ping iproute2 postgresql
COPY settings.xml /usr/share/maven/conf/settings.xml
COPY mvn-gen.sh /scripts/mvn-gen.sh
RUN echo "\nalias mvn-gen='/scripts/mvn-gen.sh'\n" >> /root/.bashrc

FROM base as discovery-server
COPY mvn-discovery-entrypoint.sh /mvn-entrypoint.sh
ENTRYPOINT ["/mvn-entrypoint.sh"]

FROM base as user-service
COPY mvn-client-entrypoint.sh /mvn-entrypoint.sh
ENTRYPOINT ["/mvn-entrypoint.sh"]